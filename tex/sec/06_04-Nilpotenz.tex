\section{Nilpotente Matrizen und Endomorphismen}
In diesem Abschnitt betrachten wir eine spezielle Klasse von Matrizen
und Endomorphismen
\begin{definition}
\begin{enumerate}
\item Sei \(V\) endl. dim VR und \(f \in End(V)\). Dann heißt \(f\)
  \emph{nilpotent}, falls 
  \(f^i =\underbrace{f \circ \hdots \circ f}_{j - \text{Mal}} = 0\)
  für ein \(j \in \mathbb{N}\). das kleinste \(j \in \mathbb{N}\) mit
  \(f^j = 0\) heißt Nilpotenzindex von \(f\).
\item Sei \(K\) ein Körper. Eine Matrix \(A \in K^{n \times n}\) heißt
  nilpotent, falls \(A^j = 0\) für ein \(j \in \mathbb{N}\). das
  kleinste \(j \in \mathbb{N}\) mit \(A^j = 0\) heißt Nilpotenzindex
  von \(A\)
\end{enumerate}
\end{definition}
\begin{example}
\(A = 
\begin{bmatrix}
0 & 1 & &0\\
& \ddots & \ddots \\
& &\ddots & 1\\
0 & & &0 \\
\end{bmatrix}
\)~ \\
\(A^i = 
\left[
\begin{array}{c|c}
0 & 
\begin{bmatrix}
  1 & & 0 \cr
  &\ddots&\cr
  0 & & 1\cr
\end{bmatrix}\\
\hline
0&0\\
\end{array}
\right]
\in K^{n \times n}, i = 0, 1, \hdots, n\) für 
\(i \in \mathbb{N}_0: Rang(A^i) = 
\begin{cases}
n-i &\text{ für } i \leq n\\
0 & \text{ sonst}
\end{cases}
\Rightarrow A^0 = I_n, 
A^{n-1} = 
\begin{bmatrix}
&&&&1\\
&&0&&\\
&&& \\
\end{bmatrix}, 
A^n = 0 \Rightarrow A\) ist nilpotent mit Index \(n\)
\end{example}

\begin{theorem}
\begin{enumerate}
\item Eine untere (obere) Dreiecksmatrix \(A \in K^{n \times n}\) ist
  genau dann nilpotent, wenn alle Diagonalelemente von \(A\) null
  sind.
\item Eine Matrix \(A\in K^{n \times n}\) ist genau dann nilpotent und
  diagonalisierbar, wenn \(A = 0\) (sie selbst schon diagonal ist)
\item Ist \(A \in K^{n \times n}\) nilpotent, so hat \(A\) nur den EW
  \(\lambda = 0\), dh \(Sp(A) = \{0\}\)
\end{enumerate}
\begin{proof}
\begin{enumerate}
\item 
\begin{itemize}
\item[\(\Rightarrow:\)]
  \begin{align*}
    A = 
    \begin{bmatrix}
      a_{1\, 1} & &*\cr
      & \ddots &\cr
      0 & & a_{n\, n}\cr
    \end{bmatrix} \in K^{n \times n} \text{nilpotent}
    & \Rightarrow \exists j \in \mathbb{N}: A^j =
      \begin{bmatrix}
        a^j_{1\,1} & &*\cr
        & \ddots &\cr 
        & & a^j_{n\, n}\cr
      \end{bmatrix} = 0\\
    &\Rightarrow a_{1\,1}\hdots a_{n\,n} = 0
  \end{align*}
\item[\(\Leftarrow:\)]
  \begin{align*}
    A = 
    \begin{bmatrix}
      0 & & *\cr
      &\ddots&\cr
      0 && 0\cr
    \end{bmatrix}
    &\Rightarrow A^j =
      \begin{array}{c|c}
        0 & 
        \begin{bmatrix}
          & *\cr
          0&\cr
        \end{bmatrix}\cr\hline
        0 & 0 
      \end{array}, j = 1, \hdots, n\\
    &\Rightarrow A^n = 0\\
    &\Rightarrow A \text{ nilpotent mit Index } \leq n
  \end{align*}
\end{itemize}
\item 
\begin{itemize}
\item [\(\Rightarrow:\)] Sei \(A\) nilpotent und diagonalisierbar
  \(\Rightarrow \exists S \in K^{n \times n}\) invertierbar
  \[S^{-1} A S = D = 
    \begin{bmatrix}
      \lambda_1 & & 0\\
      & \ddots &\\
      && \lambda_n\\
    \end{bmatrix}
  \]
  Da \(A\) nilpotent 
  \(\exists j \in \mathbb{N}: 
  A^j = 0 = (SDS^{-1})^j = SD^jS^{-1}\) 
  \[\Rightarrow 0 = D^j = 
    \begin{bmatrix}
      \lambda_1^j & & 0\\
      & \ddots &\\
      0 & & \lambda_n^j\\
    \end{bmatrix}
    \Rightarrow \lambda_1 = \hdots = \lambda_n = 0
    \]
\item [\(\Leftarrow:\)]
  \(A = 0 \Rightarrow A\) nilpotent vom Index \(1\) und
  diagonalisierbar
\end{itemize}
\item Sei \(A \in K^{n \times n}\) nilpotent
\[\Rightarrow \exists j \in \mathbb{N}: A^j = 0 \land A^{j-1} \neq 0\]
Sei \(\lambda \in Sp(A) \Rightarrow \exists v \neq 0: Av = \lambda
v\) 
\[\Rightarrow 0 = A^j\cdot v = A^{j-1}(Av) = \lambda A^{j-2}(Av) =
  \hdots = \lambda^j v \Rightarrow \lambda = 0\]
\end{enumerate}
\end{proof}
\end{theorem}
Wir wollen nun alle Formen nilpotenter Matrizen
konstruieren\footnote{Klausurrelevant}
\begin{definition}
Sei \(n \in \mathbb{N}\)
\begin{enumerate}
\item Eine Partition von \(n\) in \(s\) Teile ist eine Folge \((n_1,
  \hdots, n_s)\) mit \(n_1, \hdots, n_s \in \mathbb{N},
  \sum\limits_{i=1}^s n_i = n\) und 
  \(n_1 \geq \hdots \geq n_s \geq 1\)
\item Sei \(p = (n_1, \hdots, n_s)\) Partition von \(n\). Dann heißt 
  \((q_1, \hdots, q_t)\) mit \(t = n_1\) die, \emph{zu \(p\) duale
      Partition}, wenn \(q_i\) die Anzahl der \(n_j\) mit \(n_j \geq
  i\) aneibt, also 
  \[q_i = \left|\left\{n_j\mid n_j \geq i\right\}\right|\]
\end{enumerate}
Die Darstellung erfolgt am Besten durch ein Kästchendiagramm.
\end{definition}

\begin{example}
\begin{enumerate}
\item \(n = 16, s = 6, p = (5, 3, 3, 2, 2, 1)\) 
\[p* = (q_1,q_2,q_3,q_4,q_5) \text{ mit }
  \begin{array}{r@{=}l}
    q_1 &|\{n_j|n_j \geq 1\}| = 6\\
    q_2 &|\{n_j | n_j \geq 2| = 5\\
    q_3 & \hdots = 3\\
    q_4 & \hdots = 1\\
    q_5 & \hdots = 1\\
  \end{array}
\]
\[
  \begin{array}{c c c c c c}
    & q_1&q_2&q_3&q_4&q_5\\
    n_1 = 5&\boxed{}&\boxed{}&\boxed{}&\boxed{}&\boxed{}\\
    n_2 = 3&\boxed{}&\boxed{}&\boxed{}\\
    n_3 = 3&\boxed{}&\boxed{}&\boxed{}\\
    n_4 = 2&\boxed{}&\boxed{}\\
    n_5 = 2&\boxed{}&\boxed{}\\
    n_6 = 1&\boxed{}\\
  \end{array}
\]
\(q_j\) ist die Anzahl der Kästchen in der j-ten Spalte 
\(\Rightarrow p* = (6,5,3,1,1)\)

\item \(n=20, s=5, p=(7,5,4,2,2)\)
\[
\begin{array}{c c c c c c c c}
  & 5 & 5 & 3 & 3 & 2 & 1 & 1\\
  & q_1&q_2&q_3&q_4&q_5 & q_6 & q_7\\
  n_1 = 7&\boxed{}&\boxed{}&\boxed{}&\boxed{}&\boxed{}&\boxed{}&\boxed{}\\
  n_2 = 5&\boxed{}&\boxed{}&\boxed{}&\boxed{}&\boxed{}\\
  n_3 = 4&\boxed{}&\boxed{}&\boxed{}&\boxed{}\\
  n_4 = 2&\boxed{}&\boxed{}\\
  n_5 = 2&\boxed{}&\boxed{}\\
\end{array}
\Rightarrow p* = (5, 5, 3, 3, 2, 1, 1)
\]
\end{enumerate}
\end{example}
\begin{remark}
\((p*)* = p\)
\end{remark}

\begin{definition}
Jede Partition \(p = (n_1, \hdots, n_s)\) ordnen wir eine spezielle
nilpotente Matrix zu: 
\[N(p) = 
  \begin{bmatrix}
    N(n_1) & & 0\\
    & \ddots & \\
    0 & & N(n_s)\\
  \end{bmatrix}
  \text{ mit }
  N(n_j) = 
  \begin{bmatrix}
    0 & 1 & &0\\
    & \ddots & \ddots \\
    & &\ddots & 1\\
    0 & & &0 \\
  \end{bmatrix}
  \in K^{n_j \times n_j}, j=1, \hdots, s
\text{ nilpotent von Index } n_j\]
\(\Rightarrow N(p)\) ist nilpotent vom Index max \(n_j = n_1, 1\leq j
\leq s\), da 
\(N(p)^j = 
\begin{bmatrix}
  N(n_1)^j & & 0\\
  & \ddots & \\
  & & N(n_s)^j\\
\end{bmatrix}
\)\\
\(N(p)\) heißt die zu \(p\) gehörende nilpotente Normalform.
\end{definition}
\begin{example} \( p = (3,2,1)\)
\[
 \left[ 
  \begin{blockarray}{c c c c c c}
    \begin{block}{[c c c] c c c}
      0&1&\\
      &0&1&&0\\
      &&0\\
    \end{block}
    \begin{block}{c c c [c c] c}
      &&&0&1\\
      &0&&&0\\
    \end{block}
    \begin{block}{c c c c c [c]}
      &&&&&0\\
    \end{block}
  \end{blockarray}
  \right]
\]
\end{example}
\begin{theorem}
Sei \(n \in \mathbb{N}\) und \(p = (n_1, \hdots, n_s)\) eine Partition
von \(n\)
\begin{enumerate}
\item \(q = (q_1, \hdots, q_t)\) mit \(t = n_1\) und \(q_j =
  Rang(N(p)^{j-1}) - Rang(N(p)^j)\) für \(j = 1, \hdots, t\) ist die zu
  \(p\) duale Partition, also \(q = p*\). 
\item Ist \(\tilde{p}\) eine weitere Partition von \(n\), so gilt,
  dass \(N(p)\) und \(N(\tilde{p})\) genau dann ähnlich sind, wenn \(p
  = \tilde{p}\)
\end{enumerate}
\begin{proof}
\begin{enumerate}
\item Es gilt \(N(p)^j = 
  \begin{bmatrix}
    N(n_1)^j & &0\\
    0 & \ddots & \\
    0 & & N(n_s)^j\\
  \end{bmatrix}
  \) 
  und 
\[\underbrace{Rang(N(n_i)^{j-1})}_{
      \begin{array}{c@{:}c}
        n_i \leq j & n_i - (j-1)\\
        n_i = j-q & (j-1)-(j-1)\\
      \end{array}
  }
  -
  \underbrace{Rang(N(n_i)^j)}_{n_i \leq j: n_i -j} = 
  \begin{cases}
    1 &\text{für } n_i \leq j\\
    0 &\text{sonst}
  \end{cases}
  \text{ für } j \in \mathbb{N}, i = 1, \hdots, s
\]
Sei \(p* = (q_1, \hdots, q_t), t=n_1\) die duale Partition zu \(p\). 
\[Rang(N(p)^{j-1}) - Rang(N(p)^j) = \sum_{i=1}^sRang(N(n_i)^j) =
  |\{n_i|n_i \geq j\}| = q_j\]
\item 
\begin{itemize}
\item[\(\Leftarrow:\)] \(p = \tilde{p} \Rightarrow N(p)= N(\tilde{p})
  \Rightarrow N(p), N(\tilde{p})\) sind ähnlich
\item [\(\Rightarrow:\)] \(N(p)\) und \(N(\tilde{p})\) ähnlich
\begin{align*}
  \Rightarrow
  &\exists S \in GL_n(K): N(\tilde{p}) = S^{-1}N(p)S\\
  & N(\tilde{p})^k = (S^{-1}N(p)S)^k = S^{-1}N(p)^kS\, \forall k \in
    \mathbb{N}\\
  \Rightarrow
  &Rang(N(\tilde{p})^k) = Rang(N(p)^k)\, \forall k \in \mathbb{N}\\
  \Rightarrow
  &p* = \tilde{p}*\\
  \Rightarrow 
  &p= \tilde{p}
\end{align*}
\end{itemize}
\end{enumerate}
\end{proof}
\end{theorem}

\begin{theorem}
Sei \(A\in K^{n\times n}\) nilpotent mit Index \(t\) und \(q = (q_1,
\hdots, q_t)\) gegeben durch 
\[q_j = Rang(A^{j-1})- Rang(A^j) \text{ für } j=1, \hdots, t\]
dann ist \(q\) eine Partition von \(n\) und es existieren
invertierbare Matrizen \(S \in K^{n \times n}\), so dass 
\(S^{-1}AS = N(p)\) mit \(p = (n_1, \hdots, n_s) = q*\) und
\(n_1 = t, q_1 = s\)
\begin{proof}
\begin{enumerate}
\item \(z.z: Kern(A) \subset Kern(A^2) \subset \hdots \subset
  Kern(A^t) = K^n\)
\begin{enumerate}
\item Sei \(x \in Kern(A^j), j \in \mathbb{N}\)
\[\Rightarrow A^jx = 0 \Rightarrow A^{j+1}x = 0 \Rightarrow x \in
  Kern(A^{j+1}) \, \forall j \in \mathbb{N}\]
Insgesamt: \(Kern(A^j) \subseteq Kern(A^{j+1}), j \in \mathbb{N}\)
\item Da \(A^t = 0\) ist \(Kern(A^t) = K^n\)
\item Sei \(k \in \mathbb{N}\) die kleinste Zahl, so, dass 
\(Kern(A^k) = Kern(A^{k+1})\)\\
Sei \(x \in Kern(A^{k+2})\) 
\begin{align*}
  \Rightarrow
  &0 = K^{n+2}x = A^{k+1}Ax\\
  \Rightarrow
  &Ax \in Kern(A^{k+1}) = Kern(A^k)\\
  \Rightarrow
  &A^k(Ax) = 0 = A^{k+1}x\\
  \Rightarrow
  &x \in Kern(A^{k+1})
\end{align*}
Insgesamt: 
\begin{align*}
  Kern(A^k) 
  &= Kern(A^{k+1}) = Kern(A^{k+2}) = \hdots = Kern(A^t)\\
  \Rightarrow
  &A^k = \hdots = A^t = 0\\
  \Rightarrow
  &k = t \text{ da } A^{t-1} \neq 0 \text{ wegen Minimalität von } t
    \text{ und } k
\end{align*}
\end{enumerate}
Insgesamt: 
\begin{align*}
  dim(Kern(A^j)) - dim(Kern(A^{j-1})) 
  &= n - Rang(A^j) - n + Rang(A^{j-1})\\
  &= q_j \geq 1, j = 1, \hdots, t\\
  \Rightarrow q_1 
  &= dim(Kern(A))
\end{align*}
\item Sei für \(t \geq j \geq 2 (v_1, \hdots, v_{k_j})\) eine Basis
  von \(Kern(A^{j-1}) \subset Kern(A^j)\) und 
  \((v_1, \hdots, v_{k_j}, w_1,\hdots, w_{q_j})\) eine Basis von 
  \(Kern(A^j)\)\\
\(z.z: A{w_1}, \hdots, A{w_{q_j}} \in Kern(A^{j-1})\backslash
Kern(A^{j-2})\) linear unabhängig
\begin{align*}
  w_l \in Kern(A^t)
  &\Rightarrow 0 = A^jw_l = A^{j-1}A{w_l}\\
  &\Rightarrow A{w_l} \in Kern(A^{j-1})
\end{align*}
\(A^{j-2}(Aw_l) = A^{j-1}w_l \neq 0\), da 
\(w_l \notin Kern(A^{j-1})\), aufgrund der Konstruktion. \\
Insgesamt: 
\[w_l, l = (1, \hdots, q_j) \in Kern(A^{j-1}) \backslash Kern(A^{j-2})\]
Sei 
\(\alpha_1Aw_1 + \hdots + \alpha_{q_j}Aw_{q_j} = 0, \alpha_l \in K\)
\begin{align*}
  \Rightarrow
  &0 = A^{j-2}\sum_{l=1}^{q_j}\alpha_l Aw_l = 
    A^{j-1} \sum_{l=1}^{q_j} \alpha_k w_l\\
  \Rightarrow
  &\sum_{l=1}^{q_j}\alpha_lw_l \in Kern(A^{j-1}) \leftarrow
    \text{ Basis von } (v_1, \hdots, v_{k_j})\\
  \Rightarrow
  &\exists \gamma_i, i = 1, \hdots, k_j \text{ mit }
    \sum_{l=1}^{q_j}\alpha_l w_l = \sum_{i=1}^{q_j} \gamma_i v_i\\
  \xRightarrow{(v_1, \hdots, v_{k_j}, w_1, \hdots, w_{q_j} \text{
  Basis von } Kern(A^^j)}
  &\alpha_1 = \hdots = \alpha_{j_j} = \gamma_1 = \hdots = \gamma_{k_j}
    = 0
\end{align*}
\item \(z.z: (q_1, \hdots, q_t)\) ist eine Partition von \(n\), dh
  \(q_1 \geq \hdots \geq q_t (\geq 1)\) und 
  \(\sum\limits_{i=1}^t q_i = n\)\\
Da \(Aw_1, \hdots, Aw_{q_j} \in Kern(A^{j-1})Kern(A^j)\) linear
unabhängig
\[\Rightarrow q_{j-1} = dim(Kern(A^{j-1})) - dim(Kern(A^j)) \geq q_j\]
\begin{align*}
  \sum_{j=1}^t q_j 
  &= \sum_{j-1}^t dim(Kern(A^j)) - dim(Kern(A^{j-1}))\\
  &= dim(Kern(A^t)) - dim(Kern(A^0)) = n-0 = n
\end{align*}
\item Konstruktion der Transformationsmatrix \(S\)
\[
\def\arraystretch{1.1}
\begin{blockarray}{c c c c c}
  Kern(A) \subset \hdots \subset 
  &Kern(A^j) \subset \hdots \subset
  &Kern(A^{t-1}) \subset \hdots \subset
  &Kern(A^t) = K^n
  &\\
  \begin{block}{c c c c\} c}
    v_{1,1}:= Av_{1,2}
    & v_{1, j}:= Av_{j, j+1}
    & v_{1, t-1}:= Av_{1, t}
    & v_{1, t} \\
    \vdots
    &\vdots
    &\vdots
    &\vdots
    &\text{linear unabhängig}\\
    v_{q_2,1}:= Av_{q2,2}
    & v_{q_{j+1}, j}:= Av_{q_{j+1}, j+1}
    & v_{q_{t}, t-1}:= Av_{q_t, t}
    & v_{q_t, t} \\
\end{block}
v_{q_2 +1, 1} 
& v_{q_{j+1}+1, j}
& v_{q_t +1, t-1}\\
\vdots 
&\vdots 
&\vdots \\
v_{q_1, 1} 
& v_{q_j, j}
&v_{q_{t-1}, t-1}\\
&&&&\\
&(1)&(2)&(3)
\end{blockarray}
\]
\begin{tabular}{c|c}
\hline
\((1)\) & Basiserweiterung von \(Kern(A^{j-1})\rightarrow
          Kern(A^j)\)\\
\((2)\) & Basiserweiterung von \(Kern(A^{t-2}) \rightarrow
          Kern(A^{t-1})\)\\
\((3)\) & Basiserweiterung von \(Kern(A^{t-1}) \rightarrow Kern(A^t)\)
\end{tabular}
\begin{itemize}
\item[\(j = t:\)] 
Sei \((v_{1,t}, \hdots, v_{q_t, t})\) eine Basiserweiterung von
\(Kern(A^{t-1})\) zu \(Kern(A^t)\)
\[\Rightarrow v_{i, t-1} := Av_{i,t} \in Kern(A^{t-1})\backslash
  Kern(A^{t-2}) \text{ linear unabhängig; für } i = 1, \hdots, q_t\]
\item[\(j = t-1:\)] Sei 
  \((\underbrace{v_{1,t}, \hdots, v_{q_t,t}}
  _{\text{von oben}}, v_{q_{t+1, t-1}}, \hdots
  ,v_{q_{t-1, t-1}})\) eine Basiserweiterung von \(Kern(A^{t-2})\) zu
  \(Kern(A^{t-1})\)
\item[\(j:\)] Sei 
  \((\underbrace{v_{1,t}, \hdots, v_{q_{j+1},j}}
  _{\text{von oben}}, v_{q_{j+1} + 1, j}, \hdots, v_{q_j, j})\) 
  eine Basiserweiterung von \(Kern(A^{j-1})\) zu \(Kern(A^j)\)
  \begin{align*}
    \Rightarrow
    &v_{i, j-1} := A \cdot v_{i, j} \in Kern(A^{j-1}) \backslash
      Kern(A^{j-2}) \text{ linear unabhängig; für } i = 1, \hdots,
      a_j\\
    \Rightarrow
    &v_{j, 1}:= Av_{j, 2} \in Kern(A)\backslash\{0\} \text{ linear
      unabhängig; für } i = 1, \hdots, q_2
  \end{align*}
\item[\(j=1:\)] Sei 
  \((\underbrace{v_{1,t}, \hdots, v_{q_{2},1}}
  _{\text{von oben}}, v_{q_2, 1}, \hdots, v_{q_1, 1})\) Basis von
  \(Kern(A)\)~\\
  Die Vektoren \(v_{1, 1}, \hdots, v_{q_r, t}\) sind linear
  unabhängig, da \((v_{1,j}, \hdots, v_{q_j, j})\) immer eine
  Basiserweiterung von \(Kern(A^{j-1})\) zu \(Kern(A^j)\) sind, also
  insbesondere in \(Kern(A^j) \backslash Kern(A^{j-1})\).\\
  Es sind \(\sum\limits_{j=1}^t q_j = n\) Stück solche \(v_{j,i}\),
  sie spannen also den \(K^n\) auf \(\Rightarrow (v_{1,1}, \hdots,
  v_{q_t, t}\) Basis von \(K^n\)
\end{itemize}
Insgesamt: \(S = (v_{1,1}, \hdots, v_{1, n_1} | v_{2, 1}, \hdots,
v_{2, n_2}| \hdots |v_{q_1}, \hdots, v_{q_1, n_{q_1}})\) ist
invertierbar.
\[A \cdot S \stackrel{(1)}{=} S\left[ 
  \begin{blockarray}{c c c c c c c c c c c c c c c c c}
    \begin{block}{[c c c c] c c c c c c c c c c c c\} c}
      0 & 1\\
      & \ddots & \ddots &&&&&&&&&&&&&& n_1\\
      && 0 & 1\\
    \end{block}
    \begin{block}{c c c [c c c c] c c c c c c c c c\} c}
      &&&0 & 1\\
      &&&& \ddots & \ddots &&&&&&&&&&& n_2\\
      &&&&& 0 & 1\\
    \end{block}
    &&&&&&&\ddots\\
    \begin{block}{c c c c c c c c c [c c c c] c c c\} c}
      &&&&&&&&&0 & 1\\
      &&&&&&&&&& \ddots & \ddots &&&&& n_q\\
      &&&&&&&&&&& 0 & 1\\
    \end{block}
  \end{blockarray}
  \right]
= S \cdot N(p)
\]
\((1):a_{v_j, 1} = (0, v_{1,1}, \hdots, v_{1, n_1-1}|0, v_{2,1},
\hdots, v_{2, n_2 -1}| \hdots |0, v_{q_1, 1}, \hdots, v_{q_1, n_{q_1-1}})\) 
restliche shiften \(v_{i,j} \rightarrow v_{i, j -1}\)
\end{enumerate}
\end{proof}
\end{theorem}
\begin{example}
\begin{itemize}
\item[] \(A = 
\begin{bmatrix}
-1 & 1 & 2\\
-3 & 3 & -6\\
-1 & 1 & -2\\
\end{bmatrix}
\in \mathbb{R}^{3\times 3}, Rang(A) = 1\) 
\item[]\(A^2 = 0 \Rightarrow \) Nilpotenzindex\(x = 2\)
\begin{align*}
  \Rightarrow a(0) &= 3\\
  \Rightarrow g(0) &= dim(Kern(\underbrace{A - 0I}_A)) = 2
\end{align*}
\item[]\(q_1 = dim(Kern(A) = 2, q_2 = dim(Kern(A^2)) - dim(Kern(A)) =
  1\)~\\
  \(\Rightarrow (2,1)* = (1,2)\)
\item[] \(Kern(A) = Span\left\{
    \begin{pmatrix}
      1\\ 3\\ 1\\
    \end{pmatrix}, 
    \begin{pmatrix}
      0 \\ 2 \\ 1\\
    \end{pmatrix}
    \right\}
    \subset Kern(A^2) = \mathbb{R}^3\) 
\[
\left.
\begin{array}{c c}
  v_{1,1} = A_{v_1, 2} &= \begin{pmatrix}1\cr 3\cr 1\cr\end{pmatrix}\\
  v_{2,1}  &= \begin{pmatrix}0\cr 2\cr 1\cr \end{pmatrix}
\end{array}
\right\}
\Rightarrow
\left[
\begin{array}{c c | c}
  1 & 0 & 0\\
  3 & 3 & 2\\
  1 & 1 & 1\\
\end{array}
\right]
\Rightarrow
S^{-1}AS =
\left[
\begin{array}{c c | c}
  0 & 1 & 0\\
  0 & 0 & 0\\
  \hline
  0 & 0 & 0\\
\end{array}
\right]
\]
\end{itemize}
\end{example}



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Lineare_Algebra_2"
%%% End:
