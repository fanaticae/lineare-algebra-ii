\section{Bilinearformen}
\begin{definition}
Seien \(V, W K\) - Vektorräume. 
\begin{enumerate}
\item Eine Abbildung \(\beta : V \times W \rightarrow K\) heißt
  Bilinearform auf \(V \times W\), falls 
  \(\forall v, v_1, v_2 \in V, w, w_1, w_2 \in W, \lambda \in K:\) 
  \begin{enumerate}
    \item Linearität in der 1. Komponente 
      \begin{align*}
        \beta(v_1 + v_2, w) &= \beta(v_1, w) + \beta(v_2, w)\\
        \beta(\lambda v, w)&= \lambda \beta(v, w)
      \end{align*}
    \item Linearität in der 2. Komponente
      \begin{align*}
        \beta(v, w_1 + w_2) &= \beta(v, w_1) + \beta(v, w_2)\\
        \beta(v,\lambda w)&= \lambda \beta(v, w)
      \end{align*}
  \end{enumerate}
  \item \(\beta\) wie in 1. heißt \emph{nicht ausgeartet}, falls gilt: 
    \begin{enumerate}
      \item \(\beta(v,w) = 0\, \forall w \in W \Rightarrow v = 0_V\)
      \item \(\beta(v,w) = 0\, \forall v \in V \Rightarrow w = 0_W\)
    \end{enumerate}
  \item \(\beta\) wie in 1. heißt Bilinearform auf \(V\), falls 
    \(W = V\)
  \item Eine Bilinearform \(\beta: V \times V \rightarrow K\) heißt
    symmetrisch, falls 
    \(\beta(v, w) = \beta(w, v)\, \forall v, w \in V\)
  \item Eine Bilinearform \(\beta:V \times V \rightarrow \mathbb{R}\) heißt
    positiv definit, falls 
    \(\beta(v, v) > 0 \forall v \in V\backslash\{0\}\)
  \item Eine Bilinearform \(\beta: V\times V \rightarrow \mathbb{R}\)
    heißt positiv semidefinit, falls 
    \(\beta(v, v) \geq 0\, \forall v \in V\) 
  \item Eine Matrix \(A \in \mathbb{R}^{n \times n}\) heißt positiv
    definiert (bzw positiv semidefinit), falls 
    \(v^T A v > 0 \forall v \in \mathbb{R}^n\backslash\{0\}\)
    (bzw \(v^T A v \geq 0 \forall v \in \mathbb{R}^n\))
    \footnote{Alternative Notation - 
        positiv semidefinit: \(A \geq 0\), 
        positiv definit: \(A > 0\)
    }
  \end{enumerate}
\end{definition}
\begin{example}
\begin{enumerate}
\item \(
  \begin{array}{r l}
    det: K^2 \times K^2 &\rightarrow K\\
    (x,y) &\mapsto det\left(
            \begin{bmatrix}
              x_1 & y_1\cr
              x_2 & y_2\cr
            \end{bmatrix}
                    \right)
  \end{array}
  \) ist eine Bilinearform, die nicht ausgeartet ist.\\
  \begin{proof}
    Ist \(x \neq 0\), so lässt es sich zu einear Basis \(x, \tilde{x}\)
    von \(K^2\) ergänzen \(\Rightarrow det(x, \tilde{x}) \neq 0\).
    \begin{enumerate}
    \item \(det(x,y) = 0 \forall y \in K^2 \Rightarrow x = 0\) 
    \item \(det(x,y) = 0 \forall x \in K^2 \Rightarrow y = 0\)
    \end{enumerate}
  \end{proof}
\item Sei \(A \in K^{n \times n},  
  \begin{array}{r l}
    \beta: K^m \times K^n &\rightarrow K\\
    (v,w) &\mapsto v^T A w  
  \end{array}
  \) ist bilinear 
  \begin{proof}
    \begin{enumerate}
      \item 
        \begin{align*}
          \beta(v_1 + v_2, w) &= (v_1 + v_2)^T A w = v_1^TAw =
                                \beta(v_1, w)+ \beta(v_2, w)\\
          \beta(\lambda v, w) &= (\lambda v)^R A w = \lambda(v^T A w)
                                = \lambda\beta(v,w)
        \end{align*}
      \item analog
    \end{enumerate}
  \end{proof}
\item \(\begin{array}{r l}
    \beta: V \times V^* &\rightarrow K\\
    (x, \varphi) &\mapsto \varphi(x)
  \end{array}
  \) ist eine Bilinearform
  \begin{proof}
    \begin{itemize}
      \item Linearität
        \begin{enumerate}
          \item 
            \begin{align*}
              \beta(v_1 + v_2, \varphi) &= \varphi(v_1 + v_2) =
                                          \varphi(v_1) + \varphi(v_2)
                                          = \beta(v_1, \varphi) + 
                                          \beta(v_2, \varphi)\\
              \beta(\lambda v, \varphi) &= \varphi(\lambda v) =
                                          \lambda \varphi(v) =
                                          \lambda\beta(v, \varphi)
            \end{align*}
          \item analog
        \end{enumerate}
      \item \(\beta\) nicht ausgeartet
        \begin{enumerate}
        \item \(\beta(v, \varphi) = \varphi(v) = 0\, \forall \varphi
          \in V^* \Rightarrow v = 0_v\) nach Satz 7.2
        \item \(\beta(v, \varphi) = \varphi(v) = 0\, \forall v \in V^*
          \Rightarrow \varphi = 0_{V^*}\)
        \end{enumerate}
    \end{itemize}
  \end{proof}
\end{enumerate}
\end{example}

\begin{remark}
Sei \(\beta: V \times W \rightarrow K\) eine Abbildung.
\(\beta\) ist genau dann Bilinearform auf \(V \times W\), wenn 
\begin{align*}
  \beta(\cdot, w) : V &\rightarrow K 
  &\beta(v, \cdot): W &\rightarrow K\\
  v &\mapsto \beta(v, w)
  & w \mapsto \beta(v, w)
\end{align*}
für alle \(w \in W\) und \(v \in V\) Linearformen sind.
\end{remark}
\begin{theorem}
Seien \(V, W\) endlich dimensionale K-VR. \(\beta: V\times W
\rightarrow K\) eine nicht ausgeartete Bilinearform auf \(V \times
W\). Dann sind die Abbildungen
\begin{align*}
  \gamma_1: V &\rightarrow W^* & \gamma_2: W &\rightarrow V^*\\
  v &\mapsto \beta(v,\cdot) & w &\mapsto \beta(\cdot, w)
\end{align*}
Vektorraumisomorphismen.
\begin{proof}
\begin{itemize}
\item \(z.z: \gamma_1\) linear (\(\gamma_2\) analog)\\
  Seien \(v_1, v_2 \in V, \lambda \in K\)
  \begin{align*}
    \gamma_1(v_1,v_2)(w) &=\beta(v_1 + v_2, w) = \beta(v_1, w) +
                           \beta(v_2, w) = \gamma_1(v_1)(w) + 
                           \gamma(v_2)(w) = (\gamma_1(v_1) +
                           \gamma_2(v_2))(w) \forall w \in W\\
    \Rightarrow (v_1 + v_2) &= \gamma(v_1) + \gamma(v_2)\\
    \gamma(\lambda v_1)(w) &= \beta(\lambda v_1, w) = \lambda
                             \beta(v_1, w) = \lambda \gamma(v_1)(w) =
                             (\lambda\gamma(v_1))(w) \forall w \in W\\
    \Rightarrow \gamma_1(\lambda v_1) &= \lambda \gamma_1(v_1)
  \end{align*}
\item \(z.z: \gamma_1\) injektiv (\(\gamma_2\) analog)
  \begin{align*}
    Kern(\gamma_1) &= \{v \in V| \beta(v, \cdot) = 0_{W^*}\} 
                     = \{v \in V|\beta(v,w) = 0_K \forall w \in W\}
                     = \{0\}\\
    \Rightarrow &\gamma_1 - \text{ injektiv}
  \end{align*}
\item \(z.z: \gamma_1\) surjektiv (\(\gamma_2\) analog)
\begin{align*}
  dim(V) &= dim(Kern(\gamma_1)) + dim(Bild(\gamma_1)) =
           dim(Bild(\gamma_1)) \leq dim(W^*) = dim(W)\\
  dim(W) &= dim(Kern(\gamma_2)) + dim(Bild(\gamma_2)) = 
           dim(Bild(\gamma_2))\leq dim(V*) = dim(V)\\
  \Rightarrow dim(V^*) &= dim(V) = dim(W) = dim(W^*)\\
  \Rightarrow Bild(\gamma_1) &= W^*\\
  \Rightarrow &\gamma_1 - \text{ surjektiv}
\end{align*}
\end{itemize}
\end{proof}
\end{theorem}

\begin{definition}
Sei \(\beta: V\times V \rightarrow K\) eine symetrische Bilinearform
auf V.\\
Dann heißt die Abbildung 
\(
\begin{array}{r l}
  q_\beta: V&\rightarrow K \\
  v &\mapsto \beta(v,v)
\end{array}
\) die zu \(\beta\) gehörige quadratische Form. 
\end{definition}
\begin{remark}
\begin{enumerate}
\item Sei \(2 := 1+ 1 \neq 0\). Ziel : \(\beta(v, w)\)\\
  Für alle \(v, w \in V\) gilt 
  \begin{align*}
    q_\beta (v + w) &= \beta(v+w, v+w)\\
    &= \beta(v,v) + \underbrace{\beta(w,v)}_{\beta(v,w)} 
      + \beta(v,w) + \beta(w,w)\\
    &= q_\beta(v) + 2\beta(v,w) + q_\beta(w)\\
    \Rightarrow \beta(v, w) &= \frac{1}{2}(q_\beta(v+w) - q_\beta(v) -
                              q_\beta(w))
  \end{align*}
\item \(q_\beta(\lambda v) = \beta(\lambda v, \lambda v) =
  \lambda^2q_\beta(v)\), daher quadratische Form. 
\end{enumerate}
\end{remark}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Lineare_Algebra_2"
%%% End:
