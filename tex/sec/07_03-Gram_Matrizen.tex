\section{Gram-Matrizen}
\begin{definition}
Sei \(V\) ein K-VR mit Basis \(B = (v_1, \hdots, v_n)\) und sei
\(\beta: V \times V \rightarrow K\) eine Bilinearform auf \(V\). Dann
heißt \(G_\beta := G_B(\beta) = [\beta(v_i, v_j)]_{i,j=1}^{n,n}\) die
Gram-Matrix (auch Gramsche Matrix oder Darstellende Matrix) von
\(\beta\) bzgl. \(B\).
\end{definition}

\begin{remark}
\begin{enumerate}
\item Seien \(v, w \in V \Rightarrow v = \sum\limits_{i=1}^n \lambda_i
  v_i, w = \sum\limits_{i=1}^n \mu_iv_i, \varphi_B:V\rightarrow K^n\)
\[\Rightarrow \varphi_B(v) = 
  \begin{bmatrix}
    \lambda_1\\
    \vdots\\
    \lambda_n\\
  \end{bmatrix}
  , 
  \varphi_B(w) = 
  \begin{bmatrix}
    \mu_1\\
    \vdots\\
    \mu_n\\
  \end{bmatrix}
\]
\begin{align*}
  \beta(v,w) 
  &= \beta\left(\sum_{i=1}^n\lambda_i v_i, \sum_{i=1}^n\mu_i v_i\right)\\
  &= \sum_{i=1}^n\lambda_i\left(\sum_{j=1}^n \mu_i \beta(v_i, v_j)
    \right)\\
  &= 
    \begin{bmatrix}
      \lambda_1 & \hdots & \lambda_n\cr
    \end{bmatrix}
    \begin{bmatrix}
      \sum_{j=1}^n\mu_j\beta(v_1,v_j)\cr
      \vdots\cr
      \sum_{j=1}^n \mu_j\beta(v_n,v_j)\cr
    \end{bmatrix}\\
  &=
    \begin{bmatrix}
      \lambda_1 & \hdots & \lambda_n\cr
    \end{bmatrix}
  \begin{bmatrix}
    \beta(v_1, v_1) & \hdots & \beta(v_1, v_n)\cr
    \vdots & \vdots & \vdots\cr
    \beta(v_n, v_1) & \hdots & \beta(v_n, v_n)\cr
  \end{bmatrix}
  \begin{bmatrix}
    \mu_1\cr
    \vdots\cr
    \mu_n\cr
  \end{bmatrix}\\
  &= \varphi_B(v)^T G_B(\beta) \varphi_B(w)
\end{align*}
\begin{specialcase}
\(V = K^n, B = (e_1, \hdots, e_n), v = 
\begin{bmatrix}v_1\\\vdots\\v_n\\\end{bmatrix}, 
\begin{bmatrix}w_1\\\vdots\\v_n\\\end{bmatrix}
\)
\[
  \Rightarrow \beta(v,w) = v^T \cdot G_B(\beta) \cdot w \, \forall v,w
  \in K^n
\]
\end{specialcase}
\item \(\beta: V \times V\rightarrow K\) ist symmetrisch
  \(\Leftrightarrow G_B(\beta)\) ist symmetrisch
\item \(\beta: V \times V\rightarrow K\) ist nicht ausgeartet
  \(\Leftrightarrow G_B(\beta)\) ist invertierbar
\item \(\beta: V \times V\rightarrow K\) ist positiv definiert
  \(\Leftrightarrow G_B(\beta)\) ist positiv definiert
\end{enumerate}
\end{remark}
\begin{example}
\(V = \mathbb{R}, v = \mathbb{R}_1[t]\) mit Basis \((1,t)\)\\
\[\beta(p,q) = \int_{-1}^1 p(t) q(t) dt - \text{ Bilinearform}\]
\begin{align*}
\beta(1,1) &= \int_{-1}^1 1 dt = 2\\
\beta(1,t) &= \beta(t, 1) = \int_{-1}^1t dt =
             \left. \frac{t^2}{2}\right|_{-1}^1 = \frac{1}{2} -
             \frac{1}{2} = 0\\
\beta(t,t) &= \int_{-1}^1 t^2 dt = \left. \frac{t^3}{3}\right|_{-1}^1
             = \frac{2}{3}\\
\end{align*}
\(\Rightarrow G_B(\beta) = \begin{bmatrix}2 & 0\\ 0 &
  \frac{2}{3}\end{bmatrix}\)~\\
Seien \(p = (2+t), q=(1-t) \Rightarrow 
\varphi_B(p) = \begin{bmatrix}2\\1\\\end{bmatrix}
\varphi_B(q) = \begin{bmatrix}1\\-1\\\end{bmatrix}
\) 
\[\beta(p,q) = \lambda^T G_B(\beta)\mu =
  \begin{bmatrix} 
    2 & 1
  \end{bmatrix}
  \begin{bmatrix}
    2 & 0\\
    0 & \frac{2}{3}\\
  \end{bmatrix}
  \begin{bmatrix}
    1\\
    -1\\
  \end{bmatrix}
  = \frac{10}{3}, 
  \beta(p,q) = 
  \int_{-1}^1 (2+t)(1-t) dt = \hdots = \frac{10}{3}
\]
\end{example}
\begin{lemma}
Seien \(A, C \in K^{m\times n}\). Dann gilt 
\[
  x^TAy = x^T Cy \forall x \in K^m, \forall y \in K^n 
  \Leftrightarrow
  A = C
\]
\begin{proof}
\begin{itemize}
\item[\(\Leftarrow\)] trivial
\item[\(\Rightarrow\)] \(A = [a_{ij}], C=[c_{ij}]\)
\[\Rightarrow a_{ij} = e_{i,m}^T A e_{j,n} = e_{i,m}^T C e_{j,n} =
  c_{ij}\]
\end{itemize}
\end{proof}
\end{lemma}

\begin{theorem}
Sei \(V\) K-VR mit Basen 
\(B_1 = (v_1, \hdots, v_n), B_2 = (w_1, \hdots, w_n)\) und \(S =
[id_V]^{B_1}_{B_2}\) die Basisübergangsmatrix von \(B_1\) nach
\(B_2\).
Dann gilt: 
\[G_{B_1}(\beta) = S^TG_{B_2}(\beta)S\]
\begin{proof}
Seien \(v, w \in V, \lambda = \varphi_{B_1}(v), \mu \varphi_{B_1}(w)\)
\begin{align*}
\varphi_{B_2}(v) &= [id]_{B_2}^{B_1}\varphi_{B_1}(v) = S \lambda\\
\varphi_{B_2}(v) &= [id]_{B_2}^{B_1}\varphi_{B_1}(w) = S \mu
\end{align*}
\[\lambda^T G_{B_1}(\beta)\mu = \beta(v,w) ) (S\lambda)^T G_{B_2}
  (\beta) S \mu = \lambda^T(S^T G_{B_2} (\beta) S) \mu \, \forall
  \lambda, \mu \in K^n\]
(da \(v, w\) beliebig und \(\varphi_{B_1}\) bijektiv)
\(\Rightarrow G_{B_1}(\beta) = S^TG_{B_2}(\beta)S\)
\end{proof}
\end{theorem}
\begin{definition}
Zwei Matrizen \(A,C \in K^{n\times n}\) heißen kongruent, falls es
eine invertierbare Matrix \(S \in K^{n\times n}\) gibt, so dass 
\(C = S^T A S\)
\end{definition}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Lineare_Algebra_2"
%%% End:
