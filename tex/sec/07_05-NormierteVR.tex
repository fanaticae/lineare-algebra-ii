\section{Normierte Vektorräume}
\begin{definition}
\begin{enumerate}
\item Sei \(V\) ein K-VR. Eine Abbildung \(||\cdot||: V \rightarrow
  \mathbb{R}\) heißt Norm auf \(V\), falls \(\forall v,w \in V,
  \lambda \in K:\)
  \begin{enumerate}
    \item \(||v|| >0\) und \(||v|| = 0 \Leftrightarrow v = 0\) 
    \item \(||\lambda v|| = |\lambda| ||v||\) 
    \item \(||v+w|| \geq ||v|| + ||w||\)
  \end{enumerate}
\item Ein K-VR zusammen mit einer Norm 
  \(||\cdot||: V\rightarrow \mathbb{R}\) heißt normierter Vektorraum
\end{enumerate}
\end{definition}
\begin{example}
\begin{enumerate}
\item \(V = \mathbb{R},
  |v| = ||v|| = 
  \begin{cases}
    x & x \geq 0\\
    -x& x < 0
  \end{cases}
\)
\item \(V = \mathbb{C}, |v| = ||v|| = ||a + ib|| = \sqrt{a^2 + b^2}\)
\item \(||\cdot||_p : K^n \rightarrow \mathbb{R}\) mit \(||p|| =
  \left(\sum\limits_{i=1}^n |x_i|\right)^{\frac{1}{p}}\) für 
  \(x \in K^n, p \in \mathbb{R}, p \geq 1\) ist eine Norm (p-Norm) auf
  \(K^n\). 
  \begin{enumerate}
    \item \(||x||_p \geq 0 (||x|| = 0 \Leftrightarrow x = 0)\)
    \item \(||\lambda x||_p = \left(\sum_{i=1}^n|\lambda
        x_i|^p\right)^{\frac{1}{p}} = |\lambda| \left( \sum_{i=1}^n
        |x_i|^p\right)^{\frac{1}{p}} = |\lambda| ||x||_p\)
    \item folgt aus der Minkovskiungleichung
      \[||x+y||_p 
        =\left(\sum_{i=1}^n |x_i + y_i|\right)^{\frac{1}{n}}
        \leq 
        \left(\sum_{i=1}^n |x_i|\right)^{\frac{1}{n}}
        + 
        \left(\sum_{i=1}^n |y_i|\right)^{\frac{1}{n}}
        = ||x||_p + ||y||_p
      \]
  \end{enumerate}
\begin{specialcase}
\begin{itemize}
\item \(p = 1, ||x||_1 = |x_1| + \hdots + |x_n|\)
  Taxi-Norm\\
  \footnote{Denn das Taxi fährt nie den direkten Weg, sondern
      immer einen Umweg}

\item \(p = 2, ||x||_2 = \sqrt{|x_1|^2 + \hdots + |x_n|^2}\)
  Euklidische Norm auch für \(K = \mathbb{C}\)

{\centering
  \includegraphics[width=5cm]{img/07-03-taxinorm.pdf}
}
\begin{example}
\(x = \begin{bmatrix}4\\3\end{bmatrix}, y
= \begin{bmatrix}5\\1\end{bmatrix} \in \mathbb{R}^2\)
\begin{align*}
||x||_1 = (4 + 3) = 7 & > 6 = 5 + 1 = ||y_1||\\
||x||_2 = \sqrt{4^2+3^2} = \sqrt{25} & < \sqrt{26} = \sqrt{25 + 1} =
                                       ||y||
\end{align*}
\end{example}
\end{itemize}
\end{specialcase}
\item \(||\cdot||_\infty:K^n\rightarrow \mathbb{R}\) mit
  \(||x||_\infty = \max\limits_{1 \leq i \leq n} |x_i|\) für 
  \(x \in K^n\) ist eine Norm, denn \(\forall x, y \in K^n, \lambda
  \in K\): 
  \begin{enumerate}
  \item \(||x||_\infty \geq 0, ||x||_\infty = 0 \leftrightarrow x = 0\)
  \item \(||\lambda x||_\infty = \max|\lambda x_i| = |lambda|\max|x_i|
    = |\lambda| ||x||_\infty\)
  \item \(||x + y||_\infty = \max\limits_{1 \leq i \leq n} |x_i + y_i|
    \leq \max\limits_{1 \leq i \leq n}|x_i| + \max\limits_{1 \leq i
        \leq n} |y_i| = ||x||_\infty + ||y||_\infty\)
  \end{enumerate}
\end{enumerate}
\end{example}
\begin{target}
Zeige, dass \(||v|| := \sqrt{<v,v>}\) eine Norm ist, wenn \(<\cdot,
\cdot>\) ein Skalarprodukt ist
\end{target}
\begin{theorem}[Cauchy-Schwarz-Ungleichung]
Sei \(V\) ein euklidischer oder unitärer Vektorraum mit Skalarprodukt
\(<\cdot, \cdot>\). Dann gilt für alle \(v, w \in V:\)
\[|<v,w>|^2 \leq <v,v>\cdot <w,w>\]
Gleichheit gilt genau dann, wenn \(v, w\) linear abhängig sind.
\begin{proof}
Für \(w = 0\) gilt \(|<v,w>|^2 = 0 = <v,v><w,w>\)\\
Sei \(w \neq 0\)
\begin{align*}
\Rightarrow 0 
&\leq <v-\lambda w, v-\lambda w> \\
&=<v,v> - \overline{\lambda}\underbrace{<w,v>}_{\overline{<v,w>}}
 - \lambda <v,w> + \overline{\lambda}\lambda <w,w>\\
&= <v,v> - 2 Re(\lambda<v,w>) + \overline{\lambda}\lambda <w,w>
&&\forall \lambda \in K
\end{align*}
Setze \(\lambda = \frac{\overline{<v,w>}}{<w,w>}\)
\begin{align*}
\Rightarrow 0 
&\leq <v,v> - \frac{<v,w>}{<w,w>}\overline{<v,w>} -
  \frac{\overline{<v,w>}}{<w,w>} <v,w> 
  +\frac{<v,w>\overline{<v,w>}}{<w,w>\cancel{<w,w>}}\cancel{<w,w>}\\
&=<v,v> - \frac{\overline{<v,w>}}{<w,w>} <v,w>
\end{align*}\footnote{Forme um auf \(\overline{<v,w>}<v,w>\)}
\[|<v,w>|^2 = \overline{<v,w>}<v,w> \leq <v,v><w,w>\]
\begin{itemize}
\item[\(\Leftarrow\)] Sind \(v,w\) linear abhängig (und \(w \neq 0\)),
  so gibt es \(\lambda \in K: v = \lambda w\)
\begin{align*}
\Rightarrow |<v,w>|^2 &= \overline{<v,w>}<v,w> \\
  &= \overline{<\lambda w, w>} <v,w> \\
  &= \lambda <w,w> <v,w> \\
  &= <v,\lambda w><w,w>\\
  &= <v,v><w,w>
\end{align*}
\item[\(\Rightarrow\)] Sei \(|<v,w>|^2 = <v,v><w,w>\)
\[0 \leq <v - \lambda w, v-\lambda w> = <v,v> - 
  \frac{\overline{<v,w>}<v,w>}{<w,w>} = 
  <v,v> - \frac{|<v,w>|^2}{<w,w>} = 0\]
\begin{align*}
\Rightarrow 
&<v - \lambda w, v-\lambda w> = 0\\
\Rightarrow 
&v - \lambda w = 0\\
\Rightarrow 
&v = \lambda w\\
\Rightarrow
& v,w \text{ linear abhängig}
\end{align*}
\end{itemize}
\end{proof}
\end{theorem}

\begin{theorem}
Sei \(V\) ein euklidischer oder unitärer Vektorraum mit Skalarprodukt
\(<\cdot,\cdot>\). Dann ist 
\(||\cdot||: V \rightarrow \mathbb{R}, v \mapsto \sqrt{<v,v>}\) eine
Norm auf \(V\) (die durch das Skalarprodukt unreduzierte Norm
\begin{proof}
\begin{enumerate}
\item \(||v|| = \sqrt{<v,v>} \geq 0 , ||v|| = 0 \Leftrightarrow <v,v>
  = 0 \Leftrightarrow v = 0\)
\item \(||\lambda v|| = \sqrt{<\lambda v, \lambda v>} = 
  \sqrt{\overline{\lambda}\lambda <v,v>} = 
  |\lambda| ||<v,v>||\)
\item 
  \begin{align*}
    ||v+w||^2 
    &= <v+w,v+w>\\
    &=<v,v> + <v,w> + \underbrace{<w,v>}_{<v,w>} + <w,w>\\
    &= ||v||^2 + 2 Re(<v,w>) + ||w||^2\\
    &\leq ||v||^2 + 2|<v,w>| + ||w||^2\\
    &\leq ||v||^2 + 2||v||||w|| + ||w||^2\\
    &= (||v|| +  ||w||)^2\\
    \Rightarrow
    &||v + w|| \leq ||v||
  \end{align*}
\end{enumerate}
\end{proof}
\end{theorem}
\begin{example}
\begin{enumerate}
\item \(V = K^n, ||v||_2 = \sqrt{\sum\limits_{i=1}^n |v_i|^2} = 
  \sqrt{\sum\limits_{i=1}^n \overline{v}_i v_i} = 
  \sqrt{v^* v} = \sqrt{<v,v>}\)
\item \(V = K^{n \times m}, ||A||_F := \sqrt{\sum_{i=1}^n \sum_{j=1}^m
      |a_{ij}|^2}\) Frobeniusnorm auf \(K^n\) ist die durch das
  Frobenius-Skalarprodukt \(<A,B> = \sqrt{Spur(A^{\ast}B)}\)
 iduzierte Norm
\end{enumerate}
\end{example}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Lineare_Algebra_2"
%%% End:
