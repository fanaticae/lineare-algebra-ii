\section{Orthogonale und unitäre Endomorphismen}
\begin{definition}
Sei \(f \in End(V, V)\). \(f\) heißt orthogonal 
(im Fall \(K = \mathbb{R}\)) bzw unitär 
(im Fall \(K = \mathbb{C}\)), falls 
\[\forall v, w \in V: <f(v), f(w)> = <v,w>\]
\end{definition}

\begin{theorem}
Sei \(f \in End(V,V)\) orthogonal bzw unitär.
Dann gilt
\begin{enumerate}
\item \(f\) ist injektiv
\item \(f\) ist isometrisch, dh \(||f(v)|| = ||v|| \, \forall v \in
  V\)
\item Ist \(\lambda \in K\) ein Eigenwert von \(f\), so gilt 
  \(|\lambda| = 1\). 
\item Ist \(dim(V) < \infty\), so ist \(f\) bijektiv und 
  \(f^{ad} = f^{-1}\)
\end{enumerate}
\begin{proof}
\begin{enumerate}
\item Sei \(f(v_1) = f(v_2)\) für \(v_1, v_2 \in V\)
\begin{align*}
\Rightarrow 
  &0 = f(v_1 - v_2)\\
\Rightarrow
  &0 = <f(v_1-v_2),f(v_1-v_2)> = <v_1 -v_2, v_1 - v_2>\\
\Rightarrow 
  &||v_1 - v_2||^2 = 0\\
\Rightarrow & v_1 = v_2
\end{align*}
\item \(||f(v)|| = \sqrt{<f(v), f(v)>} = \sqrt{<v,v>} = ||v||\)
\item Sei \(\lambda \in K\) Eigenwert von \(f\) und \(v \in
  V\backslash\{0\}\) mit \(f(v) = \lambda v\). 
\[<v,v> = <f(v),f(v)> = <\lambda v, \lambda v> = 
  \overline{\lambda}\lambda <v,v> = |\lambda|^2<v,v>
  \Rightarrow |\lambda| = 1\]
\item Da \(dim(V) < \infty \Rightarrow f\) surjektiv \(\Rightarrow f\)
  bijektiv.
\[<f^{-1}(w),v> = <f(f^{-1}(w)), f(v)> = <w, f(v)>\Rightarrow f^{-1}= f^{ad}\]
\end{enumerate}
\end{proof}
\end{theorem}

\begin{remark}
Ist \(dim(V) < \infty\), so ist \(G_1 = \{f \in \mathscr{L}(V,V)
  | f \text{ orthogonal}\}\) bzw \(G_2 = \{f \in \mathscr{L}(V,V)| f
  \text{ unitär}\}\) eine Untergruppe von \(S(V):= \{f: V \rightarrow
  V| f \text{ bijektiv}\), denn für alle \(f, g \in G_1\) gilt
  (\(G_2\) analog):
  \begin{itemize}
  \item \(Id_V \in G_1 \Rightarrow G_1 \neq \varnothing\)
  \item \(f \circ g \in G_1\), denn für alle \(v, w \in V\) gilt: 
    \begin{align*}
      <(f \circ g)(v), (f\circ g)(w)> 
      &= <f(g(v)), f(g(w))>\\
      &= <g(v), g(w)> \\
      &= <v, w\\
      \Rightarrow & f\circ g \in G_1
    \end{align*}
  \item \(f^{-1} \in G_1\), denn \(\forall v, w \in V: <f^{-1}(v),
    f^{-1}(w)> = <f(f^{-1}(v)), f(f^{-1}(w))> = <v, w>\)
  \end{itemize}
\end{remark}

\begin{example}
\(V = \mathbb{R}^2, R_\alpha: \mathbb{R}^2 \rightarrow \mathbb{R}^2\)
(Drehung um \(\alpha\). 
\begin{align*}
\Rightarrow& R_\alpha \text{ erhält Längen und Winkel}\\
\Rightarrow& <R_\alpha(x), R_\alpha(y)> =
             ||R_\alpha(x)|| ||R_\alpha(y)|| \cos\varphi = ||x|| ||y||
             \cos\varphi  = <x,y>
\end{align*}
Sei \(B = (e_1, e_2)\) kanonische Basis
\begin{align*}
\Rightarrow
  R_\alpha(e_1) &= 
    \begin{bmatrix}
      \cos{(\alpha)}\\
      \sin{(\alpha)}\\
    \end{bmatrix} = \cos{(\alpha)}e_1 + \sin{(\alpha)}e_2\\
  R_\alpha(e_2) &=
    \begin{bmatrix}
      -\sin{\alpha}\\
      \cos{\alpha}\\
    \end{bmatrix}
  = -\sin{(\alpha)} e_1 + \cos{(\alpha)} e_2\\
A_\alpha
  &= [R_\alpha]^{B_1}_{B_1}  = 
    \begin{bmatrix}
      \cos{\alpha} &-\sin{\alpha}\\
      \sin{\alpha} &\cos{\alpha}
    \end{bmatrix}
\end{align*}
\begin{align*}
  det(\lambda I - A_\alpha) &= \lambda^2 - 2\cos{(\alpha)}\lambda  1 =
                             0\\
  \Rightarrow \lambda_{1,2} &= \cos\alpha \pm i \sin\alpha\\
  \Rightarrow |\lambda_{1,2}|&= 1\\
  \Rightarrow A_0 &= I_2\\
  \Rightarrow A_\pi &= -I_2
\end{align*}
\begin{itemize}
\item \(A_\alpha^T A_\alpha = 
  \begin{bmatrix}
    \cos\alpha & \sin\alpha\\
    -\sin\alpha& \cos\alpha\\
  \end{bmatrix}
  \begin{bmatrix}
    \cos\alpha & -\sin\alpha\\
    \sin\alpha & \cos\alpha\\
  \end{bmatrix}
  = 
  \begin{bmatrix}
    1 & 0\\
    0 & 1\\
  \end{bmatrix}\)
\item \(A_\alpha^{-1}
  = 
  \begin{bmatrix}
    \cos\alpha & \sin\alpha\\
    -\sin\alpha& \cos\alpha\\
  \end{bmatrix}
  = A_\alpha^T
  = 
  \begin{bmatrix}
    \cos{(-\alpha)} & -\sin{(-\alpha)}\\
    \sin{-\alpha} & \cos{(-\alpha)}\\
  \end{bmatrix}\)
\end{itemize}
\end{example}

\begin{definition}
\begin{enumerate}
\item Eine Matrix \(A \in \mathbb{R}^{n \times n}\) heißt orthogonal
  (bzgl des Standardskalarprodukts), falls \(A^T A = I\) 
\item Eine Matrix \(A \in \mathbb{C}^{n\times n}\) heißt unitär, falls
  \(A^*\cdot A = I\)
\end{enumerate}
\end{definition}

\begin{remark}
\begin{enumerate}
\item \(A\) orthogonal \(\Leftrightarrow\) \(A\) invertierbar und
  \(A^{-1} = A^T \Leftrightarrow AA^T = I\)\\
  Achtung: \(A \in \mathbb{R}^{m \times n}, m > n\) und 
  \(A^T \cdot A = I_n\) aber \(A A^T \neq I_m\)\\
  \(A = \begin{bmatrix}1\\0\end{bmatrix}, A^T\cdot A = [1], AA^T 
  \begin{bmatrix}
    1 & 0\\
    0 & 0\\
  \end{bmatrix}
  \)
\item \(A\) unitär \(\Leftrightarrow\) \(A\) invertierbar und 
  \(A^{-1} = A^*\)
\item \(A\) orthogonal\(\Rightarrow det(A) = 1\)
\item \(A\) unitär \(\Rightarrow |det(A)| = 1\)
\item \(A \in K^{n \times n}\) orthogonal (bzw unitär)
\[
\begin{array}{r l c}
\Rightarrow A:K^n &\rightarrow K^n & \text{ orthogonal (bzw unitär)}\\
v &\mapsto Av & \text{ denn } <Av, Aw> = (Av)^*Aw = v^*A^*Aw = v^*w
                =<v,w>
\end{array}
\]

\item 
  \begin{itemize}
  \item \(O(n) = 
    \{A \in \mathbb{R}^{n \times n} | A \text{ orthogonal} \}\) "= Orthogonale Gruppe 
  \item \(U(n) = \{A \in \mathbb{R}^{n \times n} | A 
    \text{ unitär}\}\) "= unitäre Gruppe
  \item \(SO(n) = \{A \in O| det(A) = 1\}\) spezielle Orthogonale
    Gruppe 
  \end{itemize}

\end{enumerate}
sind Untergruppen \(GL_n(K)\)
\end{remark}

\begin{theorem}
Sei \(B = (v_1, \hdots, v_n)\) eine ONB von \(V\) und \(f \in
End(V)\), dann gilt:\\

\begin{center}
\(f\) orthogonal bzw unitär \(\Leftrightarrow\) \([f]_B^B\) orthogonal
bzw unitär.
\end{center}

\begin{proof}
\(([f]^B_B)^*[f]^B_B = [f^{ad}]_B^B[f]_B^B = [f^{ad} \circ f]^B_B\)
\begin{align*}
[f]^B_B \text{ orthogonal (unitär)}
&\Leftrightarrow [f^{ad} \circ f]_B^B = I\\
&\Leftrightarrow f^{ad} \circ f = id_V\\
&\Leftrightarrow f^{ad} = f^{-1}\\
&\Leftrightarrow f \text{ orthogonal bzw unitär}
\end{align*}
\end{proof}

\end{theorem}



\begin{theorem}
Seien \(B_1 = (v_1, \hdots, v_n)\) und \(B_2 = (u_1, \hdots, u_n)\)
ONB von \(V\). Dann ist \([id_V]_{B_2}^{B_1}\) orthogonal bzw unitär.
\begin{proof}
\begin{align*}
v_j &= \sum_{i=1}^n a_{ij} u_i & j = & 1, \hdots, n\\
u_j &= \sum_{i=1}^n c_{ij} v_i & j = & 1, \hdots, n
\end{align*}
\begin{align*}
a_{kj} 
&= \sum_{i=1}^na_{ij}<u_{ki}, u_i>\\
&= <u_km \sum_{i=1}^n a_{ij} u_i>\\
&= <u_k, v_j>\\
&= <\overline{v_j, u_k}>\\
&= <\overline{v_j \sum_{i=1}^n c_{ik}v_i}>\\
&= <\overline{v_j, \sum_{i=1}^n c_{ik} v_i}>\\
&= \overline{\sum_{i=1}^n c_{ik} <v_j, v_i>}\\
&= \overline{v_{jk}}\\
\Rightarrow V &= A^* = A^{-1}\\
\Rightarrow A & \text{ orthogonal (unitär)}
\end{align*}
\end{proof}
\end{theorem}

\begin{theorem}
\begin{enumerate}
\item Sei \(dim(V) < \infty\) und \(f \in \mathscr{L}(V,V)\) unitär,
  dann gibt es eine ONB von \(V\) bestehend aus Eigenvektoren von
  \(f\). Speziell ist \(f\) diagonalisierbar. 
\item Sei \(A \in \mathbb{C}^{n \times n}\) unitär. Dann gibt es eine
  unitäre Matrix \(U \in \mathbb{C}^{n\times n}\)  und 
  \(\lambda_1, \hdots, \lambda_n \in \mathbb{C}\) mit \(|\lambda_i| =
  1, i = 1, \hdots, n\) so, dass \(U^{-1} A U = U^*A U = 
  \begin{bmatrix}
    \lambda_1\\
    &\ddots\\
    &&\lambda_n
  \end{bmatrix}
\)
\end{enumerate}
\begin{proof}
\begin{enumerate}
\item \(p_f = det(\lambda id_V - f) = (\lambda - \lambda_1) \hdots
  (\lambda-\lambda_n)\) nach Satz 8.6 gilt 
  \(|\lambda_i| = 1, i = 1, \hdots, n\)\\
{\bf Induktion nach \(n = dim(V)\)}
\begin{itemize}
\item \(n = 1\): Sei \(v_1 \in V\backslash\{0\}\)EV von \(f\) zum EW
  \(\lambda_1 \Rightarrow B = \left(\frac{v_1}{||v_1||}\right)\) "=
  ONB aus EV von \(f\)
\item \(n - 1 \mapsto n\): Sei \(v_1 \in V\backslash\{0\}\)
  Eigenvektor von \(f\) zum Eigenwert \(\lambda_1\) und \(||v_1|| =
  1, W = Span\{v_1\}^\perp \Rightarrow dim(W) = n-1, 
  f(w): V \rightarrow W\)\\
\(z.z: W\) ist \(f\)-invariant, dh \(f(W) \subseteq W\)\\
Sei \(w \in W\)
\begin{align*}
\Rightarrow
&\lambda_1<f(w), v_1> = <f(v), \lambda_1 v_1 = <f(w), f(v_1)> = <w, v>
  = 0\\
\Rightarrow
&<f(w), v> = 0, \text{ da } |\lambda_1| = 1 \neq 0\\
\Rightarrow 
& f(w) \in W\\
\Rightarrow 
&W f-\text{invariant}
\end{align*}
\(f|_W : W \rightarrow W\) unitär, da \(W, <\cdot, \cdot >|_W\)
unitärer Vektorraum
\begin{align*}
\Rightarrow
&B_W = (v_2 \hdots v_n) \text{ ONB von } W, v_j \text{ Eigenvektoren
  von } f, j = 2\hdots n\\
\Rightarrow
&B = (v_1\hdots v_n) \text{ ONB von } V, v_j \text{ Eigenvektoren von
  } f, j = 1\hdots n
\end{align*}
\end{itemize}
\item Sei \(A\in \mathbb{C}^{n\times n}\) unitär, 
\(A: \mathbb{C}^{n\times n} \rightarrow \mathbb{C}^{n\times n}, v
\mapsto Av\) unitär.\\
Wegen 1. gibt es eine ONB \(B = (v_1\hdots v_n)\) von \(\mathbb{C}_n\)
mit Eigenvektor \(v_j\) von \(A\). \\
Sei \(E = (e_1 \hdots e_n)\Rightarrow [A]_E^E = A\).
\[[A]_B^B = 
  \begin{bmatrix}
    \lambda_1 & & 0\\
    &\ddots\\
    0&&\lambda_n
  \end{bmatrix}
  \text{ da } Av_j = \lambda_j v_j = \sum_{i=1}^n a_i v_i
\]
\end{enumerate}
Sei \(U = [id_{\mathbb{C}_n}]^B_E \Rightarrow U\) invertierbar. 
\[[A]_B^B = [id_{\mathbb{C}_n}]_B^E [A]_E^E [id_{\mathbb{C}_n}]_E^B =
  U^{-1}AU = U^*AU\]
\end{proof}
\end{theorem}

\begin{lemma}
Sei \(f \in \mathscr{L}(V, V)\) orthogonal und \(dim(V) = 2\). 
Dann gibt es eine Orthogonalbasis \(B\) von \(V\), so dass 
\[[f]_B^B = 
  \begin{bmatrix}
    \cos\alpha & -\sin\alpha\\
    \sin\alpha & \cos\alpha\\
  \end{bmatrix}
  \text{ oder }
  [f]_B^B = 
  \begin{bmatrix}
    1 & 0\\
    0 & -1\\
  \end{bmatrix}
  \text{ mit } \alpha \in [0, 2\pi)\]
\begin{proof}
Es gibt eine ONB \(\tilde{B}\) von \(V\).
\begin{align*}
\Rightarrow
&A := [f]^{\tilde{b}}_{\tilde{b}} = 
  \begin{bmatrix}
    a & b\\
    c & d\\
  \end{bmatrix}\\
\Rightarrow
&
\begin{bmatrix}
  1 & 0\\
  0 & 1\\
\end{bmatrix}
= A^TA = 
\begin{bmatrix}
a & c\\
b & d\\
\end{bmatrix}
\begin{bmatrix}
a & b\\
c & d\\
\end{bmatrix}
= 
\begin{bmatrix}
a^2 + c^2 & ab + cd\\
ab + cd & b^2 + d^2\\
\end{bmatrix}\\
\Rightarrow 
&a^2+c^2 = 1, b^2 + d^2 = 1\\
\Rightarrow 
&\exists\alpha,\beta \in [0, 2\pi): 
  a = \cos\alpha, b = \sin\beta, c = \sin\alpha, d = \cos\beta\\
\Rightarrow
&\cos\alpha\sin\beta + \sin\alpha\cos\beta = \sin(\alpha + \beta) =
  0\\
\Rightarrow
&\alpha + \beta = k\pi, k \in \mathbb{Z}\\
\Rightarrow &
b = \sin\beta = \sin(k\pi - \alpha) = -(-1)^k\sin\alpha,
d = \cos\beta = \cos(k\pi - \alpha) = (-1)^k \cos\alpha
\end{align*}
\begin{itemize}
\item {\bf Fall 1:}\(k\) gerade: 
\[\Rightarrow A =
\begin{bmatrix}
\cos\alpha & -\sin\alpha\\
\sin\alpha & \cos\alpha\\
\end{bmatrix}
\Rightarrow \tilde{B} \text{ gesuchte Basis}
\]
\item {\bf Fall 2:} \(k\) ungerade:
\[\Rightarrow A = 
  \begin{bmatrix}
    \cos\alpha & \sin\alpha\\
    \sin\alpha & -\cos\alpha\\
  \end{bmatrix}
\]
\[p_A = det(\lambda I - A) = \lambda^2 - 1 = (\lambda - 1)(\lambda +
  1) \Rightarrow \lambda_1 = 1, \lambda_2 = -1 \text{ EW von } f\]
Seien \(v_1, v_2 \in V \backslash\{0\}\) die zugehörigen Eigenvektoren
mit \(||v_1|| = 1 =||v_2||\)
\begin{align*}
\Rightarrow 
&<v_1, v_2> = <f(v_1), f(v_2)> = <\lambda_1v_1, \lambda_2 v_2> =
  -<v_1,v_2>\\
\Rightarrow <v_1, v_2>
&= 0\\
\Rightarrow 
&B = (v_1, v_2) - \text{ ONB von } V \text{ mit }
[f]_B^B = 
\begin{bmatrix}
1 & 0\\
0 & -1\\
\end{bmatrix}
\end{align*}
\end{itemize}
\end{proof}
\end{lemma}
\begin{remark}
\begin{align*}
S: \mathbb{R}^2 &\rightarrow \mathbb{R}^2\\
\begin{bmatrix}
u_1\\
u_2\\
\end{bmatrix}
&\mapsto
\begin{bmatrix}
1 & 0\\
0 &-1\\
\end{bmatrix}
\begin{bmatrix}
u_1\\
u_2\\
\end{bmatrix}
= 
\begin{bmatrix}
u_1\\
-u_2\\
\end{bmatrix}
\end{align*}
Spiegelung an der durch
\(e_1\) aufgespannten Gerade
\end{remark}

\begin{theorem}
\begin{enumerate}
\item Sei \(dim(V) < \infty\) und \(f \in \mathscr{L}(V,V)\)
orthogonal. Dann gibt es eine ONB \(B\) von \(V\), so dass 
\[[f]_B^B = 
\begin{bmatrix}
I_k\\
&-I_m\\
&&A_1\\
&&&\ddots\\
&&&&A_r
\end{bmatrix}
\text{ mit } A_i =
\begin{bmatrix}
\cos\alpha_i & -\sin\alpha_i\\
\sin\alpha_i & \cos\alpha_i\\
\end{bmatrix}
\text{ mit }
\alpha_i \in [0, 2\pi), i = 1, \hdots, r
\]
\item Sei \(A \in \mathbb{R}^{n\times n}\) orthogonal, dann gibt es
  eine orthogonale Matrix \(A \in \mathbb{R}^{n\times n}\), so dass
  \(Q^{-1} A  Q = Q^T A Q = 
  \begin{bmatrix}
    I_k\\
    &-I_m\\
    &&A_1\\
    &&&\ddots\\
    &&&&A_r
  \end{bmatrix}
\) mit \(A_i\) wie in 1. Die Form der Matrix nenne \((*)\)
\end{enumerate}
\begin{proof}%Schwarzmagie
\(z.z:\) Es gibt einen \(f\) invarianten Untervektorraum 
\(W \subseteq V\) mit \(1 \leq dim(W) \leq 2\)\\
Sei \(\tilde{B}\) eine Basis von \(V\) und \(A =
[f]_{\tilde{B}}^{\tilde{B}} \in \mathbb{R}^{n \times n}\). Dann gilt
\(\varphi_{\tilde{B}} \circ f = A \circ \varphi_{\tilde{B}}\) 
\begin{itemize}
\item {\bf Fall 1:} \(A\) hat einen reellen Eigenwert \(\lambda\).\\
Sei \(v \in \mathbb{R}^n\backslash\{0\}\) ein Eigenvektor von \(A\) zu
\(\lambda\).\\
\(\Rightarrow \tilde{W}) = Span\{v\}\) ist \(A\)-invariant und
\(dim(\tilde{W}) = 1\)
\item {\bf Fall 2:} \(A\) hat keine reellen Eigenwerte. Sei \(\lambda
  = a + ib \in \mathbb{C}, a,b \in \mathbb{R}\) Eigenwert und sei 
  \(v = u + iw \in \mathbb{C}^n\backslash\{0\}, u, w \in \mathbb{R}^n\)
  ein EV von \(A\) zu \(\lambda\).
\begin{align*}
\Rightarrow 
&Av = \lambda v\\
\Rightarrow
& Au + i Aw = A(u + iw) = (a + ib)(u + iw) = (au - bw) + i(bu + aw)\\
\Rightarrow 
&Au = au-bw, Aw = bu + aw\\
\Rightarrow 
&\tilde{W} = Span\{u, v\} A\text{-invariant und } 1 \leq
  dim(\tilde{W}) \leq 2\\
\Rightarrow 
&W = \varphi_{tilde{B}}^{-1}(\tilde{W}) \subseteq V\\
\Rightarrow &
1 \leq dim(W) \leq 2\\
\Rightarrow &
w \in W: \varphi_B(w) \in \tilde{W}\\
\Rightarrow &
A\varphi_B(w) \in \tilde{W}\\
\Rightarrow 
&\varphi_{\tilde{B}}(f(w)) \in \tilde{W}\\
\Rightarrow f(w) = \varphi_{\tilde{B}}^{-1}(\tilde{w}) \in W\\
\Rightarrow W f-\text{invariant}
\end{align*}
Induktion nach \(n = dim(V)\): 
\begin{itemize}
\item \(n = 1\): Sei \(v \in V\backslash\{0\}\), dh \(V = Span\{v\}\)
\begin{align*}
\Rightarrow 
&f(v) = \lambda v \text{ für ein }\lambda \in \mathbb{R}\\
\Rightarrow 
&\lambda \text{ Eigenwert von } f \text{ mit dem Eigenvektor } v\\
\Rightarrow
&\lambda = \pm 1\\
\Rightarrow
&f = id_V \text{ oder } f = -id_V\\
\Rightarrow
&[f]_{\tilde{B}}^{\tilde{B}} = \pm 1 \text{ f+ rdie ONB } 
  \tilde{B} = \frac{v}{||v||}
\end{align*}
\item \(n = 2\): Nach Lemma 8.10 gibt es eine ONB \(\tilde{B}\) so,
  dass 
  \[[f]_{\tilde{B}}^{\tilde{B}} = 
    \begin{bmatrix}
      \cos\alpha_1 & -\sin\alpha_1\\
      \sin\alpha_1 & \cos\alpha_1\\
    \end{bmatrix}
    = A_1
  \]
  oder 
  \[[f]^{\tilde{B}}_{\tilde{B}} = 
    \begin{bmatrix}
      1 & 0\\
      0 & -1\\
    \end{bmatrix}
  \]
\item \(n\): Sei \(dim(V) = n\) 
  \begin{align*}
    \Rightarrow 
    & W \subseteq V f-\text{ invarianter UVR mit } 1 \leq dim(W) \leq 2\\
    \Rightarrow 
    & dim(W)^\perp < n, \text{ da } W \obot W^\perp = V
  \end{align*}
\[f(W) = W = f^{-1}(W), \text{ da } W f-\text{ invariant, dh } f(W)
  \subseteq W, f \text{ bijektiv}\]
\(z.z: W^\perp f\)-invariant\\
\[\forall w \in W: <v,f(w)> = <f^{ad}(v),w> = 
  <\underbrace{f^{-1}v}_{\in W}, \underbrace{w}_{\in W^\perp}> = 0\,
  \forall v \in V\
\]
\begin{align*}
\Rightarrow
&f(w)  \in W^\perp\\
\Rightarrow
&W^\perp f\text{-invariant}\\
\Rightarrow 
&f|_{W^\perp} : W^\perp \rightarrow W^\perp \text{
  orthogonal}\\
\Rightarrow
& \text{ nach IV gibt es eine ONB }B_\perp \text{ von } W^\perp, 
  \text{ so, dass } [f|_{W^\perp}]_{B_\perp}^{B_\perp} 
  \text{ die Form } (*) \text{ hat}
\end{align*}
Ergänze \(B_\perp\) durch \(\tilde{B}\) zu einer ONB von \(V\), so hat
\([f]_B^B, B = (\tilde{B}, B_\perp)\) nach eventueller Umnummerierung
der Basisvektoren die Form \((*)\).
\end{itemize}
\item Folgt aus 1. (Übung)
\end{itemize}
\end{proof}
\end{theorem}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Lineare_Algebra_2"
%%% End:
