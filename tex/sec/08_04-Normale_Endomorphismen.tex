\section{Normale Endomorphismen}
\begin{definition} Sei \(dim(V) < \infty\)
\begin{enumerate}
\item \(f \in End(V)\) heißt normal, falls \(f^{ad}\circ f = f \circ
  f^{ad}\)
\item \(A \in K^{n \times n}\) heißt normal, falls \(A^* A = A A^*\)
\end{enumerate}
\end{definition}

\begin{remark}
\begin{enumerate}
\item Ist \(B\) eine ONB von \(V\) und \(f \in End(V)\), so gilt: 
\[f \text{ normal } \Leftrightarrow [f]_B^B \text{ normal}\]
\item \(U \in K^{n \times n}\) orthogonal bzw. unitär und \(A \in K^{n
      \times n}\), so gilt: 
\[A \text{ normal }\Leftrightarrow U^* A U \text{ normal}\]
\begin{proof}
\begin{itemize}
\item[\(\Rightarrow\)] 
  \begin{align*}
    (U^* A U)^* (U^* A U) 
    &= U^* A \underbrace{U^{*^*}U^*}_I A U\\
    &= U^* A^* A U\\
    &= U^* A A^* U \\
    &= U^* A U U^* A^* U\\
    &= U^* A U (U^* A U)^*
  \end{align*}
\item[\(\Leftarrow\)]
\[U^*A^*AU = (U^*AU)^* (U^*AU) = U^*AU (U^*AU)^* = U^* A A^* U
  \Rightarrow A^* A = A A^* \Rightarrow A \text{ normal}\]
\end{itemize}
\end{proof}
\item  \(A = \begin{bmatrix} A_1\\&A_2\end{bmatrix}\) - normal
  \(Leftrightarrow A_1, A_2\) normal
\end{enumerate}
\end{remark}

\begin{example}
\begin{enumerate}
\item \(I_n\) normal 
\item \(f\) orthogonal (unitär) \(\Rightarrow f\) normal\\
  \(f\) selbstadjungiert \(\Rightarrow f\) normal
\item \(A^* = A\) oder \(A^* = A^{-1} \Rightarrow A\) normal
\item \(A \in \mathbb{R}^{n \times n}\) schiefsymmetrisch,
  dh \(A^T = -A\) \(\Rightarrow A\) normal und alle Eigenwerte von
  \(A\) sind rein imaginär.
\begin{proof}
\[A^T A = -A A = A (-A) = A A^T \Rightarrow A \text{ normal}\]
Sei \(\lambda \in \mathbb{C}\) Eigenwert von \(A\), dh 
\(Av = \lambda v\) für ein \(v \neq 0\)
\begin{align*}
  \Rightarrow & \overline{\lambda} v^* = v^*A^* = \underbrace{v^*A^T}_{-v^*A}\\
  \Rightarrow & \lambda v^* v = v^* A v = \overline{\lambda} v^* v\\
  \Rightarrow & \lambda = - \overline{\lambda}\\
  \Rightarrow & Re(\lambda) = 0
\end{align*}
\end{proof}
\item \(A \in \mathbb{C}^{n \times n}\) ist schief-hermitssch, dh
  \(A^* = -A\)  \(\Rightarrow A \) normal und alle Eigenwerte von
  \(A\) sind rein imaginär
\item \(A\) diagonal \(\Rightarrow A\) normal
\end{enumerate}
\end{example}

\begin{theorem}
Sei \(dim(V) < \infty\) und \(f \in End(V)\) normal. Dann gilt
\begin{enumerate}
\item \(Kern(f) = Kern(f^{ad})\)
\item Für alle Eigenwerte aus \(K\) von \(f\) gilt: 
  \[Kern(f - \lambda id_V) = Eig_\lambda(f) 
    = Eig_{\overline{\lambda}}(f^{ad}) = Kern(f^{ad} -
    \overline{\lambda} id_V)\]
\end{enumerate}
\begin{proof}
\begin{enumerate}
\item Für alle \(v \in V:\) 
  \begin{align*}
    <f(v), f(v)> 
    &= <f^{ad}(f(v)), v>\\
    &= <f(f^{ad}(v)), v>\\
    &=\overline{ <v, f(f^{ad}(v)>}\\
    &=\overline{<f^{ad}(v), f^{ad}(v)>}\\
    &= <f^{ad}(v), f^{ad}(v)>
  \end{align*}
  \begin{align*}
    v \in Kern (f) \Leftrightarrow
    & f(v) = 0\\
    \Leftrightarrow
    & <f(v), f(v)> = 0\\
    \Leftrightarrow 
    & <f^{ad}(v), f^{æd}(v)> = 0\\
    \Leftrightarrow 
    & f^{ad}(v) = 0\\
    \Leftrightarrow 
    & v \in Kern(f^{ad})
  \end{align*}
\item Sei \(g = f - \lambda id_v \Rightarrow g^{ad} = f^{ad} -
  \overline{\lambda} id_V\) 
  \[\Rightarrow Eig_\lambda(f) = Kern(g) = Kern(g^{ad}) 
    = Eig_{\overline{\lambda}}(f^{ad})\]
\end{enumerate}
\end{proof}
\end{theorem}

\begin{theorem}
\begin{enumerate}
\item Seien \(dim(V) < \infty\), \(f \in End(V)\) normal und alle
  Eigenwerte von \(f\) in \(K\). 
  Dann gibt es eine ONB von \(V\) bestehen aus Eigenvektoren von
  \(f\). Speziell gilt: \(f\) diagonalisierbar. 
\item Sei \(A \in \mathbb{R}^{n \times n}\) normal. Dann gibt es eine
  unitäre Matrix \(U \in K^{n \times n}\), so dass 
  \(U^* A U = \begin{bmatrix}
    \lambda_1\\&\ddots\\&&\lambda_n\end{bmatrix}, \lambda_j \in
  \mathbb{C} EW, j = 1,\hdots, n \)
\item Sei \(A \in \mathbb{R}^{n \times n}\) normal. Dann gibt es eine
  orthogonale Matrix \(Q \in \mathbb{R}^{n \times n}\) so, dass 
  \(Q^T A Q = \begin{bmatrix}A_1\\&\ddots\\&&A_k\end{bmatrix}\), wobei
  \(A_j \in \mathbb{R}\) oder \(A_j \in \mathbb{R}^{2 \times 2}\) mit
  Eigenwerten \(a_j \pm i \beta_j\) und \(\beta_j \neq 0\)
\end{enumerate}
\begin{proof}(Übung)
\end{proof}
\end{theorem}
\pagebreak
\begin{multicols}{2}
\[K = \mathbb{C}\]
\begin{itemize}
\item \(A\) hermitesch\\
EW sind reell\\
ONB aus EV
\item \(A\) unitär \(A^* A = id = A A^*\)\\
EW \(|\lambda| = 1\)\\
ONB aus EV
\item \(A\) schief hermitesch \(A^* = -A\)
EW sind imaginär\\
ONB aus EV
\end{itemize}
\columnbreak


\[K = \mathbb{R}\]
\begin{itemize}
\item \(A\) symmetrisch, \(A^T = A\)\\
Eigenwerte sind reell\\
ONB aus reellen EW
\item \(A\) orthogonal \(A^TA ) I = A A^T\)\\
EW \(|\lambda| = 1\)\\
ONB aus komplexen EV
\item \(A\) schiefsymmetrisch, \(A^T = -A\)\\
Eigenwerte sind rein imaginär\\
ONB aus komplexen EV
\end{itemize}
\end{multicols}




%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Lineare_Algebra_2"
%%% End:
